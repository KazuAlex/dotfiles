lua <<EOF
local monokai = require('monokai')
local palette = monokai.classic
monokai.setup {
  palette = {
    base2 = '#1b1d1e',
  },
  custom_hlgroups = {
    CtrlPMatch = {
      fg = palette.orange,
    },
  },
}
EOF
