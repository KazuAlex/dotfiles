let g:ctrlsf_regex_pattern = 1

let g:ctrlsf_default_root = 'project'

let g:ctrlsf_auto_focus= {
    \ "at": "done",
    \ "duration_less_than": 1000
    \ }

let g:ctrlsf_search_mode = 'async'

let g:ctrlsf_winsize = '30%'
