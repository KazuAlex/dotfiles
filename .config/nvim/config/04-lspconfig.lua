local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, 'lua/?.lua')
table.insert(runtime_path, 'lua/?/init.lua')

local lspconfig = require('lspconfig')

lspconfig.lua_ls.setup({
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
        path = runtime_path,
      },
      diagnostics = {
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file('', true),
        checkThirdParty = false,
        preloadFileSize = 2000,
      },
      telemetry = {
        enable = false,
      },
    }
  },
})

local handle = io.popen("npm list -g | head -1")
local npm_result = ''
if nil ~= handle then
  npm_result = handle:read("*a")
  handle:close()
end
local npm_path = string.gsub(npm_result, '%s+$', '')
local angularls_cmd = {
  npm_path .. '/node_modules/@angular/language-server/bin/ngserver',
  '--tsProbeLocations', './node_modules',
  '--ngProbeLocations', npm_path .. '/node_modules/@angular/language-server',
  '--stdio',
};
lspconfig.angularls.setup({
  cmd = angularls_cmd,
  on_new_config = function(new_config)
    new_config.cmd = angularls_cmd
  end,
})

--[[ Begin: format-ts-errors stuff ]]--
lspconfig.tsserver.setup({
  handlers = {
    ["textDocument/publishDiagnostics"] = function(
      _,
      result,
      ctx,
      config
    )
      if result.diagnostics == nil then
        return
      end

      -- ignore some tsserver diagnostics
      local idx = 1
      while idx <= #result.diagnostics do
        local entry = result.diagnostics[idx]

        local formatter = require('format-ts-errors')[entry.code]
        entry.message = formatter and formatter(entry.message) or entry.message

        -- codes: https://github.com/microsoft/TypeScript/blob/main/src/compiler/diagnosticMessages.json
        if entry.code == 80001 then
          -- { message = "File is a CommonJS module; it may be converted to an ES module.", }
          table.remove(result.diagnostics, idx)
        else
          idx = idx + 1
        end
      end

      vim.lsp.diagnostic.on_publish_diagnostics(
        _,
        result,
        ctx,
        config
      )
    end,
  },
})
--[[ End: format-ts-errors stuff ]]--
