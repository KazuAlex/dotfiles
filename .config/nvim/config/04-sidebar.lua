require('sidebar-nvim').setup({
  open = false,
  initial_width = 25,
  sections = { 'diagnostics', 'symbols', 'git' },
  section_separator = { '-----' },

  git = {
    icon = "",
  },

  diagnostics = {
    icon = "",
  },

  symbols = {
    icon = "ƒ",
  },
})
