local configpath = vim.fn.stdpath('config')
local pluginspath = configpath .. '/plugins'

require('lazy').setup({
  -- allow to customize startup screen
  {
    'startup-nvim/startup.nvim',
    dependencies = {
      'nvim-telescope/telescope.nvim',
      'nvim-lua/plenary.nvim'
    },
  },

  -- file explorer
  'nvim-tree/nvim-web-devicons',
  'nvim-tree/nvim-tree.lua',

  -- bottom status line (like powerline)
  {
    'nvim-lualine/lualine.nvim',
    dependencies = {
      'nvim-tree/nvim-web-devicons',
      'linrongbin16/lsp-progress.nvim',
    },
  },

  -- display filename on the top of the split
  -- 'b0o/incline.nvim',

  -- fuzzy finder
  {
    'nvim-telescope/telescope.nvim',
    dependencies = { 'nvim-lua/plenary.nvim' },
  },
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
  },

  -- coc.nvim
  {
    'neoclide/coc.nvim',
    branch = 'master',
    build = 'npx yarn install --frozen-lockfile',
  },

  -- commentary utils
  'terrortylor/nvim-comment',

  -- auto insert closing delimiter
  'Raimondi/delimitMate',

  -- allow to read or write files with "sudo"
  'lambdalisue/suda.vim',

  -- able to surround
  'tpope/vim-surround',

  -- read editorconfig
  'editorconfig/editorconfig-vim',

  -- add a scrollbar
  'dstein64/nvim-scrollview',

  -- language parser
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    version = '0.9.2'
  },

  -- display an indent guide
  {
    'lukas-reineke/indent-blankline.nvim',
    main = 'ibl',
  },

  -- LSP
  {
    'autozimu/LanguageClient-neovim',
    build = 'bash install.sh',
  },
  'neovim/nvim-lspconfig',

  -- format ts errors
  {
    'davidosomething/format-ts-errors.nvim',
    dependencies = {
      'neovim/nvim-lspconfig',
    },
  },

  -- multiple cursor
  'mg979/vim-visual-multi',

  -- motion
  'ggandor/leap.nvim',

  -- LSP enhanced UI
  {
    'nvimdev/lspsaga.nvim',
    event = 'LspAttach',
    dependencies = {
      { 'nvim-tree/nvim-web-devicons' },
      -- Please make sure you install markdown and markdown_inline parser
      { 'nvim-treesitter/nvim-treesitter' },
    },
  },

  -- Display a popup with possible key bindings of the command you started typing
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = { },
  },

  -- Show lsp progress status
  {
    'linrongbin16/lsp-progress.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
      require('lsp-progress').setup()
    end,
  },

  -- Custom sidebar
  'sidebar-nvim/sidebar.nvim',

  -- Color preview
  {
    'NvChad/nvim-colorizer.lua',
    config = function()
      require('colorizer').setup()
    end,
  },

  {
    'folke/noice.nvim',
    event = 'VeryLazy',
    dependencies = {
      'MunifTanjim/nui.nvim',
      'nvim-notify',
      'rcarriga/nvim-notify',
    },
  },

  -- Session manager
  {
    'rmagatti/auto-session',
    config = function()
      require('auto-session').setup({
        auto_restore_enabled = false
      })
    end
  },
  {
    'rmagatti/session-lens',
    dependencies = {
      'rmagatti/auto-session',
      'nvim-telescope/telescope.nvim',
    },
  },

  -- high contrast colorscheme
  {
    'scottmckendry/cyberdream.nvim',
    lazy = false,
    priority = 1000,
    config = function()
        require('cyberdream').setup({
            -- Recommended - see "Configuring" below for more config options
            transparent = true,
            italic_comments = true,
            hide_fillchars = true,
            borderless_telescope = true,
        })
        vim.cmd("colorscheme cyberdream") -- set the colorscheme
    end,
  },

  -- git stuff
  'tpope/vim-fugitive',
  'TimUntersberger/neogit',
  {
    'kdheepak/lazygit.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
  },
  {
    'pwntester/octo.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
      'nvim-tree/nvim-web-devicons',
    },
  },

  -- save and restore sessions
  'mrquantumcodes/retrospect.nvim',

  --[[ Begin: WIP ]]--
  -- graphical debugger
  {
    'puremourning/vimspector',
    init = function()
      vim.g.vimspector_enable_mappings = 'VISUAL_STUDIO'
      vim.g.vimspector_install_gadgets = { 'vscode-js-debug', 'debugger-for-chrome', 'vscode-firefox-debug' }
      vim.g.vimspector_base_dir = vim.fn.expand('$HOME/.config/nvim/vimspector')
    end,
    cmd = { 'VimspectorInstall', 'VimspectorUpdate' },
    keys = {
      { '<F5>' },
      { '<F6>' },
      { '<F8>' },
      { '<F9>' },
      { '<F10>' },
      { '<F11>' },
      { '<leader>di', '<Plug>VimspectorBalloonEval', mode = { 'n', 'x' } },
    },
  },
  --[[ End: WIP ]]--
}, {
  root = pluginspath,
})

--[[ plugins to install
  call dein#add('hail2u/vim-css3-syntax')
  call dein#add('cakebaker/scss-syntax.vim')
  call dein#add('dhruvasagar/vim-table-mode')
--]]
