local find_files_options = {
  find_command = {
    'rg',
    '--files',
    '--hidden',
    '--glob',
    '!**/.git/*',
  },
}

require('telescope').setup({
  defaults = {
    cache_picker = {
      num_pickers = -1,
    },
  },
  pickers = {
    find_files = find_files_options,
  },
})

require('telescope').load_extension('fzf')
require('telescope').load_extension('session-lens')
