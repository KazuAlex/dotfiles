local highlight = {
  'Whitespace',
  'IndentBlankLine',
}

local hooks = require('ibl.hooks')
hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
  vim.api.nvim_set_hl(0, 'IndentBlankLine', { bg = '#1A1A1A' })
end)

require('ibl').setup({
  indent = { highlight = highlight, char = "" },
  whitespace = {
    highlight = highlight,
    remove_blankline_trail = false,
  },
  scope = { enabled = false },
})
