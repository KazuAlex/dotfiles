vim.g.LanguageClient_serverCommands = {
  -- cpp = {'cquery', '--log-file=,tmp/cq.log'},
  -- c = {'cquery', '--log-file=/tmp/cq.log'},
  -- python = {'/Users/aenayet/pyenv/nvim3/bin/pyls', '--log-file', '/tmp/pyls3.log', '-v'},
  -- rust = {'rustup', 'run', 'stable', 'rls'},
  -- haskell = {'hie-wrapper'},

  lua = { 'lua-language-server' },
  -- install with `npm i --location=global typescript-language-server typescript`
  typescript = { 'npx', 'typescript-language-server', '--stdio' },
  typescriptreact = { 'npx', 'typescript-language-server', '--stdio' },
  -- vue = { 'npx', 'typescript-vue-plugin' }, -- not working atm
}

vim.g.LanguageClient_autoStart = 1
vim.g.LanguageClient_rootMarkers = {
  -- cpp = {'compile_commands.json', 'build'},
  -- c = {'compile_commands.json', 'build'},
  -- haskell = {'*.cabal', 'stack.yaml'},
}

vim.opt.completefunc = 'LanguageClient#complete'
vim.opt.formatexpr = 'LanguageClient_textDocument_rangeFormatting()'

vim.g.LanguageClient_loadSettings = 1
vim.g.LanguageClient_settingsPath = '$HOME/.config/nvim/settings.json'
