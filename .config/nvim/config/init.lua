local configpath = vim.fn.stdpath('config') .. '/config'
for file in io.popen("ls -pa " .. configpath .. " | grep '.lua$' | grep -v '^init'"):lines() do
  require('config/' .. file:sub(1, -5))
end
