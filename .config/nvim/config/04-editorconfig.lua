-- ensure this plugin works well with vim-fugitive
vim.g.EditorConfig_exclude_patterns = { 'fugitive://.*' }