-- next/previeous buffers
vim.api.nvim_set_keymap('n', '<S-Tab>', ':bprevious<cr>', {silent = true, noremap = true})
vim.api.nvim_set_keymap('n', '<Tab>', ':bnext<cr>', {silent = true, noremap = true})

-- save on Ctrl + S
vim.api.nvim_set_keymap('n', '<C-s>', ':w<cr>', {silent = true, noremap = true})

-- keybinds for telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<C-p>', builtin.find_files, {}, { desc = 'Telescope - Find files' })
vim.keymap.set('n', '<C-p>p', builtin.find_files, {}, { desc = 'Telescope - Find files' })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {}, { desc = 'Telescope - Live grep' })
vim.keymap.set('n', '<C-p>f', builtin.buffers, {}, { desc = 'Telescope - Buffers' })
vim.keymap.set('n', '<leader>fc', builtin.commands, {}, { desc = 'Telescope - Commands' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {}, { desc = 'Telescope - Help tags' })
vim.keymap.set('n', '<leader>fk', builtin.keymaps, {}, { desc = 'Telescope - Keymaps' })
vim.keymap.set('n', '<leader>ff', builtin.builtin, {}, { desc = 'Telescope - Builtin' })
vim.keymap.set('n', '<leader>fr', builtin.resume, {}, { desc = 'Telescope - Resume' })
vim.keymap.set('n', '<leader>fp', builtin.pickers, {}, { desc = 'Telescope - Pickers' })
-- vim.keymap.set('n', '<leader>nf', "lua require'startup'.new_file()", {})

-- keybinds for nvim-tree
vim.api.nvim_set_keymap('n', '<C-t>', ':NvimTreeToggle<CR>', {
  silent = true,
  noremap = true,
  desc = 'NvimTree Toggle',
})

-- disable left mouse click
vim.api.nvim_set_keymap('', '<LeftMouse>', '', { silent = true })

-- language-client
-- vim.api.nvim_set_keymap('n', 'gd', ':call LanguageClient_textDocument_definition()<CR>', { silent = true, noremap = true })

-- (de)indent visual selection with tab
vim.api.nvim_set_keymap('v', '<Tab>', '>gv', {
  silent = true,
  noremap = true,
  desc = 'Indent visual selection',
})
vim.api.nvim_set_keymap('v', '<S-Tab>', '<gv', {
  silent = true,
  noremap = true,
  desc = 'Deindent visual selection',
})


-------------
-- Sidebar --
-------------
vim.keymap.set({ 'n', 'v' }, '<leader>h', '<cmd>SidebarNvimToggle<CR>', { desc = 'Toggle sidebar' })


-------------
-- lspsaga --
-------------

-- LSP finder - Find the symbol's definition
-- If there is no definition, it will instead be hidden
-- When you use an action in finder like "open vsplit",
-- you can use <C-t> to jump back
vim.keymap.set('n', 'gh', '<cmd>Lspsaga lsp_finder<CR>', { desc = "LSP Saga - Find the symbol's definition"})

-- Code action
vim.keymap.set({'n','v'}, '<leader>ca', '<cmd>Lspsaga code_action<CR>')

-- Rename all occurrences of the hovered word for the entire file
vim.keymap.set('n', 'gr', '<cmd>Lspsaga rename<CR>', { desc = 'LSP Saga - Rename all occurrences of the hovered word for the entire file' })

-- Peek definition
-- You can edit the file containing the definition in the floating window
-- It also supports open/vsplit/etc operations, do refer to "definition_action_keys"
-- It also supports tagstack
-- Use <C-t> to jump back
vim.keymap.set('n', 'gp', '<cmd>Lspsaga peek_definition<CR>', { desc = 'LSP Saga - Peek definition - Editable in floating window' })

-- Go to definition
vim.keymap.set('n','gd', '<cmd>Lspsaga goto_definition<CR>', { desc = 'LSP Saga - Go to definition'})

-- Peek type definition
-- You can edit the file containing the type definition in the floating window
-- It also supports open/vsplit/etc operations, do refer to "definition_action_keys"
-- It also supports tagstack
-- Use <C-t> to jump back
vim.keymap.set('n', 'gt', '<cmd>Lspsaga peek_type_definition<CR>', { desc = 'LSP Saga - Peek type definition - Editable in floating window' })

-- Go to type definition
-- vim.keymap.set('n','gt', '<cmd>Lspsaga goto_type_definition<CR>')


-- Show line diagnostics
-- You can pass argument ++unfocus to
-- unfocus the show_line_diagnostics floating window
vim.keymap.set('n', '<leader>sl', '<cmd>Lspsaga show_line_diagnostics<CR>', { desc = ' LSP Saga - Show line diagnostics' })

-- Show buffer diagnostics
vim.keymap.set('n', '<leader>sb', '<cmd>Lspsaga show_buf_diagnostics<CR>', { desc = 'LSP Saga - Show buffer diagnostics' })

-- Show workspace diagnostics
vim.keymap.set('n', '<leader>sw', '<cmd>Lspsaga show_workspace_diagnostics<CR>', { desc = 'LSP Saga - Show workspace diagnostics' })

-- Show cursor diagnostics
vim.keymap.set('n', '<leader>sc', '<cmd>Lspsaga show_cursor_diagnostics<CR>', { desc = 'LSP Saga - Show cursor diagnostics' })

-- Diagnostic jump
-- You can use <C-o> to jump back to your previous location
vim.keymap.set('n', '[e', '<cmd>Lspsaga diagnostic_jump_prev<CR>', { desc = 'LSP Saga - Diagnostic jump prev - <C-o> to jumb back to your previous location' })
vim.keymap.set('n', ']e', '<cmd>Lspsaga diagnostic_jump_next<CR>', { desc = 'LSP Saga - Diagnostic jump next - <C-o> to jumb back to your previous location' })

-- Diagnostic jump with filters such as only jumping to an error
vim.keymap.set('n', '[E', function()
  require('lspsaga.diagnostic'):goto_prev({ severity = vim.diagnostic.severity.ERROR })
end, { desc = 'LSP Saga - Diagnostic jumb prev - With filters' })
vim.keymap.set('n', ']E', function()
  require("lspsaga.diagnostic"):goto_next({ severity = vim.diagnostic.severity.ERROR })
end, { desc = 'LSP Saga - Diagnostic jumb next - With filters' })

-- Toggle outline
vim.keymap.set('n','<leader>o', '<cmd>Lspsaga outline<CR>', { desc = 'LSP Saga - Toggle outline' })

-- Hover Doc
-- If there is no hover doc,
-- there will be a notification stating that
-- there is no information available.
-- To disable it just use ":Lspsaga hover_doc ++quiet"
-- Pressing the key twice will enter the hover window
vim.keymap.set('n', 'K', '<cmd>Lspsaga hover_doc<CR>', { desc = 'LSP Saga - Hover doc' })

-- If you want to keep the hover window in the top right hand corner,
-- you can pass the ++keep argument
-- Note that if you use hover with ++keep, pressing this key again will
-- close the hover window. If you want to jump to the hover window
-- you should use the wincmd command "<C-w>w"
vim.keymap.set('n', '<C-K>', '<cmd>Lspsaga hover_doc ++keep<CR>', { desc = 'LSP Saga - Hover doc sticky' })

-- Call hierarchy
vim.keymap.set('n', '<Leader>ci', '<cmd>Lspsaga incoming_calls<CR>', { desc = 'LSP Saga - Incoming call hierarchy' })
vim.keymap.set('n', '<Leader>co', '<cmd>Lspsaga outgoing_calls<CR>', { desc = 'LSP Saga - Outgoing call hierarchy' })

-- Floating terminal
vim.keymap.set({'n', 't'}, '<A-d>', '<cmd>Lspsaga term_toggle<CR>', { desc = 'LSP Saga - FLoating terminal' })
