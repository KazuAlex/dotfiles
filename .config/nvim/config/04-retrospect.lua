require('retrospect').setup({
  saveKey = "<leader>\\", -- The shortcut to save the session, default is leader+backslash(\)
  loadKey = "<leader><BS>", -- The shortcut to load the session
  style = "modern" -- or "modern", if you have nui.nvim and dressing.nvim
})
