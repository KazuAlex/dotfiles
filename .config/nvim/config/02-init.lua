vim.cmd('source ' .. vim.fn.stdpath('config') .. '/colors/molokai.vim')

vim.wo.number = true -- show numbers
vim.wo.cursorline = true -- highlight current line under cursor
vim.opt.colorcolumn = "80" -- show column limit
vim.opt.scrolloff = 2 -- scroll offset - how many lines to keep above and below the cursor
vim.opt.swapfile = false -- disable swap file
vim.opt.smartindent = true -- enable smart indentation on new line
-- vim.opt.mouse = '' -- disable mouse
vim.opt.signcolumn = 'number' -- draw signcolumn over numbers column
vim.wo.foldmethod = 'syntax' -- how to detect folding
-- vim.wo.foldlevel = 1
-- vim.opt.foldlevelstart = 1
vim.wo.foldenable = 0
-- vim.wo.nofoldenable = 1

-- Tab = 2 spaces
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

-- search options
vim.opt.ignorecase = true -- ignore case for search
vim.opt.hlsearch = true -- highlight search results

vim.opt.laststatus = 3

-- set leader
vim.g.mapleader = ';'
