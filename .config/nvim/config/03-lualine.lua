require('lualine').setup({
  options = {
    -- theme = 'solarized',
    -- theme = 'moonfly',
    theme = 'material',
    -- theme = 'cyberdream',
  },
  tabline = {
    lualine_a = { 'buffers' },
    lualine_b = { },
    lualine_c = { },
    lualine_x = { },
    lualine_y = { },
    lualine_z = { 'tabs' }
  },
  sections = {
    lualine_a = { 'mode' },
    lualine_b = { 'branch', 'diff', 'diagnostics' },
    lualine_c = { 'filename', "require('lsp-progress').progress()" },
    lualine_x = { 'encoding', 'fileformat', 'filetype' },
    lualine_y = { 'progress' },
    lualine_z = { 'location' },
  },
})
