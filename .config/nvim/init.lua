-- disable netrw at the very start of your init.lua (strongly advised)
-- for nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local configpath = vim.fn.stdpath('config')
local lazypath = configpath .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ';'

package.path = package.path .. ';' .. configpath .. '/?.lua'

require('config/init')
